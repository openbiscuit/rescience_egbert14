**Reproduction of** 

Egbert M. D., Barandiaran X. E. (2014) [Modeling habits as self-sustaining patterns of sensorimotor behavior](https://www.frontiersin.org/articles/10.3389/fnhum.2014.00590/full). Frontiers in Human Neuroscience, 8(590).

[![Code DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6477279.svg)](https://doi.org/10.5281/zenodo.6477279)

# Rescience Article
Go to `article_rescience-template` and see the README to compile the article.

# Running the experiments

You first need to create a virtual environnement with the required dependencies :

	virtualenv --python=python3.6 venv

If you do not have python3.6, we also successfully experimented with python3.8 so you can instead use :

    virtualenv --python=python3.6 venv

We then activate the virtual environment

	source venv/bin/activate

You should start installing `wheel`, otherwise you would get an error (maybe just a warning actually) when installing matplotlib:

	pip install wheel==0.37.0

And install the required dependencies

	pip install -r requirements

You can then run the experiments, for example :

	cd python
	python exp-001.py

When you are done with the virtual environment, you have to unlog from it by calling

	deactivate

# Troubleshooting

If you do experience the following error 

	RuntimeError: module compiled against API version 0xe but this version of numpy is 0xd

You may have to upgrade numpy :

	pip install -U numpy

