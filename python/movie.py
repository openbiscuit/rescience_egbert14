#!/usr/bin/env python3
# coding: utf-8

# Standard modules
import logging
# External modules
import matplotlib.pyplot as plt
from matplotlib.animation import FFMpegWriter
import numpy as np
# Local modules
import history

class Movie:

    def __init__(self,
                 bounds,
                 robot_history,
                 fps=15,
                 filename='movie.mp4',
                 every=1,
                 horizon=None):
        self.bounds = bounds
        self.robot_histories = [robot_history]
        self.filename = filename
        self.every = every
        if not horizon:
            self.horizon = 0
        else:
            self.horizon = horizon
        self.movieok = False
        try:
            self.writer = FFMpegWriter(fps=fps)
            self.movieok = True
        except RuntimeError as e:
            logging.warning(f"Got an exception: {e}")
            logging.warning("I will skip movie creation")
            return
        self.fig = plt.figure()
        self.prevhistory_positions, = plt.plot([], [], color='k')
        self.history_positions, = plt.plot([], [], color='tab:blue')
        self.last_positions, = plt.plot([], [], color='tab:red')
        self.current_pos = plt.scatter([], [], s=120, color='k', zorder=2.5)
        self.text = self.fig.text(0.02, 0.9, '', fontsize=15)
        plt.gca().set_aspect('equal')
        plt.gca().set(xlim=bounds[0], ylim=bounds[1])
        self.writer.setup(self.fig, self.filename, 100)
        self.idx = 0

    def record(self, time):
        if not self.movieok:
            return

        if self.idx % self.every != 0:
            self.idx += 1
            return

        self.idx += 1

        # Plot the previous histories
        prevhist_positions = None
        for hist in self.robot_histories[:-1]:
            positions = np.array(hist.break_serie(hist.position_history,
                                                  history.Padding.NAN)[0])
            try:
                positions = positions.squeeze(-1)
            except:
                # There may be nothing to squeeze out
                pass
            if prevhist_positions is None:
                if len(positions.shape) == 1:
                    prevhist_positions = np.empty((0), float)
                elif len(positions.shape) == 2:
                    prevhist_positions = np.empty((0, 2), float)
            prevhist_positions = np.append(prevhist_positions,
                                           positions, axis=0)
        if prevhist_positions is not None:
            if len(positions.shape) == 1:
                self.prevhistory_positions.set_data(0,
                                                    prevhist_positions)
            elif len(positions.shape) == 2:
                self.prevhistory_positions.set_data(prevhist_positions[:, 0],
                                                    prevhist_positions[:, 1])
        # Plot the current history
        hist = self.robot_histories[-1]
        positions, last_positions = hist.break_serie(hist.position_history,
                                                     history.Padding.NAN)
        positions = np.array(positions)
        if last_positions is not None:
            last_positions = np.array(last_positions)
        try:
            positions = positions.squeeze(-1)
            last_positions = last_positions.squeeze(-1)
        except:
            # There may be nothing to squeeze out
            pass

        if len(positions.shape) == 1:
            self.history_positions.set_data(0,
                                            positions[-self.horizon:])
            self.current_pos.set_offsets(np.c_[0, positions[-1]])
            if last_positions is not None:
                self.last_positions.set_data(0, last_positions)
        elif len(positions.shape) == 2:
            self.history_positions.set_data(positions[-self.horizon:, 0],
                                            positions[-self.horizon:, 1])
            if last_positions is not None:
                self.last_positions.set_data(last_positions[:, 0],
                                             last_positions[: ,1])
            self.current_pos.set_offsets(positions[-1, :])

        self.text.set_text(f"t = {hist.times[-1]:.3}")
        # Plot the current position
        self.writer.grab_frame()

    def add_history(self, history):
        self.robot_histories.append(history)

    def save(self):
        if not self.movieok:
            return
        self.writer.finish()
        logging.info(f"Generated movie in {self.filename}")
        plt.close(self.fig)
