#!/usr/bin/env python3
# coding: utf-8
'''Experiment of section 3.3, revisited.

Will save/erase figures in directory exp004
Figure "exp004/trialALL.pdf" is a superposition of all Figures

Note:
In order to cope with 'periodic environment' but also with the plots
of Figure 8 in the original paper where we cannot see any discontinuities
in trajectories (like we get in plots of Figure 7), one idea is to never
'split' the trajectory but to repeat light position.

This is taken care in get_sensor_values, using modulus_pos

'''

import os

# Standard modules
import argparse
import functools
import logging
# External modules
import matplotlib.pyplot as plt
# no warning for too many opened figures
plt.rcParams.update({'figure.max_open_warning': 0})
import numpy as np
# Local modules
import robots
from history import RobotHistory, Padding
from IDSM import IDSM
from progress import ProgressBar

parser = argparse.ArgumentParser()
parser.add_argument('--dt',
                    type=float,
                    help='The time step for Euler integration',
                    default=0.01)
parser.add_argument('--Kd',
                    type=float,
                    help='Kd for setting the influence radius of a node',
                    default=1000)
parser.add_argument('--Kw',
                    type=float,
                    help='Kw constant for the weight factor function',
                    default=0.0025)
parser.add_argument('--Kt',
                    type=float,
                    help='Kt, the threshold on the density'
                         'before creating a node',
                    default=1.0)
parser.add_argument('--age_for_active',
                    type=float,
                    help='The time before a nodes influences the motor output',
                    default=10)
parser.add_argument('--rnd_step',
                    type=float,
                    help='step size for rnd init, (0.05 in Egbert14)',
                    default=0.05)
parser.add_argument('--rnd_scale',
                    type=float,
                    help='scaling Nv for rnd init, (1.00 in Egbert14)',
                    default=1.0)
parser.add_argument('--logging', choices=["info", "debug"],
                    help='Which logging level to use',
                    default="info")
parser.add_argument('--num_trials',
                    type=int,
                    required=True,
                    help='The number of trials')
parser.add_argument('--repeating_env',
                    action='store_true',
                    help='Use a repeating environmen')
parser.add_argument('--seed',
                    type=float,
                    help='The random used for numpy rng',
                    default=0)
parser.add_argument('-v', '--verbose', action="store_true",
                    help='Print verbose information')
args = parser.parse_args()

logging.basicConfig(level=logging.INFO if args.logging == "info"
                    else logging.DEBUG)

Kd = args.Kd
Kw = args.Kw
Kt = args.Kt
dt = args.dt
age_for_active = args.age_for_active
np.random.seed(args.seed)
rnd_step = args.rnd_step
rnd_scale = args.rnd_scale
#trial_number = args.trial_number
num_trials = args.num_trials
repeat_env = args.repeating_env

nsensors = 2
nmotors = 2
idsm_size = nsensors + nmotors
radius = 0.25
beta = np.pi / 6.0
position_range = (-2, 2)
env_width = 4
env_pos_offset = (2, 2)
# 5000 nodes are created with 100 walks of 50 steps
num_random_walks = 100
num_steps_per_walk = 50
trial_duration = 100


idsm = IDSM(nsensors=nsensors, nmotors=nmotors,
            Kd=Kd, Kw=Kw, Kt=Kt,
            age_for_active=age_for_active)
robot = robots.DiffDriveRobot(nsensors=nsensors,
                              radius=radius)


def norm_sensor(s):
    return s


def denorm_sensor(s):
    return s


def norm_motor(m):
    return 0.5 + 0.25 * m


def denorm_motor(m):
    return (m - 0.5)/0.25


def get_norm_sm_state(robot):
    s = robot.get_sensors()
    m = robot.get_motors()
    return np.concatenate([norm_sensor(s), norm_motor(m)])

def modulus_position( pos ):
    """ Compute modulus of robot position """
    pos += env_pos_offset
    mod_pos = np.mod( pos, env_width) - env_pos_offset
    return mod_pos

def get_sensor_values(robot, beta):
    alpha = robot.get_orientation()
    b1 = np.array([np.cos(alpha + beta), np.sin(alpha + beta)])
    b2 = np.array([np.cos(alpha - beta), np.sin(alpha - beta)])
    drange = position_range[1] - position_range[0]
    X, Y = np.meshgrid([position_range[0]- drange/2.0, 0.0, position_range[0] + drange/2.0],
                                                 [position_range[0]- drange/2.0, 0.0, position_range[0] + drange/2.0])
    light_positions = np.concatenate((X.ravel()[:, np.newaxis], Y.ravel()[:, np.newaxis]), axis=1)
    sensor_responses = []
    for b in (b1, b2):
        sensor_pos = robot.get_position() + robot.radius * b
        # Compute the normalized vector  sensor -> light
        c = light_positions - sensor_pos
        c_norm = np.linalg.norm(c, axis=1)
        b_responses = np.clip(np.sum(b * c, axis=1)/c_norm, 0.0, None)/(1.0 + c_norm**2)
        max_response = b_responses.max()
        sensor_responses.append(max_response)
    return np.array(sensor_responses) 


def idsm_motor_command(idsm, robot, t):
    '''
    Return the motor commmand given by the idsm
    '''
    global mu
    mu += dt * idsm.velocity_attraction(get_norm_sm_state(robot))
    mu = np.clip(mu, 0.0, 1.0)
    return denorm_motor(mu)


def randomize_robot(robot):
    global mu
    # Random motr state in [-1, 1]
    motor_state = -1.0 + 2 * np.random.random((nmotors,))
    # Random position within the arena
    robot.position = position_range[0] + \
            (position_range[1] - position_range[0]) * \
            np.random.random((2,))
    robot.orientation = -np.pi + 2.0 * np.pi * np.random.random()
    robot.set_motors(motor_state)
    robot.set_sensors(get_sensor_values(robot, beta))
    mu = norm_motor(robot.get_motors())


def trial(T0, duration, robot, idsm, dt, history):
    global mu
    t = T0
    # Place the robot in a random position within the arena
    randomize_robot(robot)

    # a ProgressBar, do not forget pbar.update() and pbar.finish()
    pbar = ProgressBar(maxval=duration + dt).start()
    while t < T0 + duration:
        # Compute the new motor values
        motor_state = idsm_motor_command(idsm, robot, t)
        robot.set_motors(motor_state)

        # Move the robot
        robot.step(dt)

        # Update the sensors
        robot.set_sensors(get_sensor_values(robot, beta))

        # Update the IDSM
        idsm.step(dt, get_norm_sm_state(robot))

        # Record the state of the robot
        history.record(t)
        # Let us display the number of nodes, active nodes, ..
        idsm.debug()
        t += dt
        logging.debug("t = {}".format(t))
        pbar.update(t-T0)

    pbar.finish()
    return t

# ************************************************************************* MAIN
# Test presence of "./exp004" directory, create if needed.
if not os.path.isdir( "./exp004" ):
    os.mkdir( "./exp004" )

# Random IDSM initialization
for i in range(num_random_walks):
    if not i % 10:
        logging.info("Random walk {}".format(i))
    li = np.random.random((idsm_size, ))
    for j in range(num_steps_per_walk):
        r = rnd_step*(2.0 * np.random.random((idsm_size, )) - 1.0)
        lip = np.zeros_like(li)
        for k in range(idsm_size):
            if 0 < li[k] + r[k] < 1:
                lip[k] = li[k] + r[k]
            else:
                lip[k] = li[k] - r[k]
        # Create the node at Np = li, Nv = lip - lip
        idsm.create_node(lip, rnd_scale*(lip - li))
        li = lip
logging.info("The base IDSM contains {} nodes".format(idsm.nodes.shape[0]))

# Activate all the nodes.
for n in idsm.nodes:
    n['age'] = age_for_active+1

# Run the trials
T0 = 0


# Create a new IDSM with the same nodes as the pretrained IDSM
history_robot = RobotHistory(robot)

# Run trials
for trial_number in range(num_trials):
    logging.info("Trial {}".format(trial_number))
    T0 = trial(T0, trial_duration, robot, idsm, dt, history_robot)
    logging.info("The IDSM contains {} nodes".format(idsm.nodes.shape[0]))

    
    # then display figures
    logging.info("Saving figure in {}".format('exp004/trial{:04d}.pdf'.format(trial_number+1)))
    fig = plt.figure()
    ax = fig.add_subplot(111)
    np_positions = np.array( history_robot.get_last_positions( 25.0 ) )
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(np_positions[:, 0], np_positions[:,1])
    fig.tight_layout()
    plt.savefig('exp004/trial{:04d}.pdf'.format(trial_number+1))
    plt.close(fig)

# the global figure
logging.info("Saving global figure in {}".format('exp004/trialALL.pdf'.format(trial_number)))
fig = plt.figure()
ax = fig.add_subplot(111)
np_positions = np.array( history_robot.position_history )
ax.plot(np_positions[:, 0], np_positions[:,1])
fig.tight_layout()
plt.savefig('exp004/trialALL.pdf'.format(trial_number))
plt.close(fig)
