#!/usr/bin/env python3
# coding: utf-8
'''
Experiment of section 3.2

Note:
    - I'm wondering if when using the IDSM, we should take
      the IDSM motor command as is or intergate smoothly with eq 29
'''

# Standard modules
import argparse
import functools
import logging
# External modules
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
# Local modules
import robots
from history import RobotHistory, Padding
from IDSM import IDSM
from progress import ProgressBar
from movie import Movie

parser = argparse.ArgumentParser()
parser.add_argument('--dt',
                    type=float,
                    help='The time step for Euler integration',
                    default=0.01)
parser.add_argument('--Kd',
                    type=float,
                    help='Kd for setting the influence radius of a node',
                    default=1000)
parser.add_argument('--Kw',
                    type=float,
                    help='Kw constant for the weight factor function',
                    default=0.0025)
parser.add_argument('--Kt',
                    type=float,
                    help='Kt, the threshold on the density'
                         'before creating a node',
                    default=1.0)
parser.add_argument('--age_for_active',
                    type=float,
                    help='The time before a nodes influences the motor output',
                    default=10)
parser.add_argument('--Ttest',
                    type=int,
                    help='The time for testing',
                    default=2400)
parser.add_argument('--behavior',
                    choices=['SimplePhototaxis',
                             'SinePhototaxis',
                             'Photophobia'],
                    required=True)
parser.add_argument('--logging', choices=["info", "debug"],
                    help='Which logging level to use',
                    default="info")
parser.add_argument('-m', '--movie', action="store_true",
                    default=False,
                    help='Whether to generate a movie or not (it takes time!)')
parser.add_argument('--seed',
                    type=float,
                    help='The random used for numpy rng',
                    default=0)
parser.add_argument('-v', '--verbose', action="store_true",
                    help='Print verbose information')
args = parser.parse_args()

logging.basicConfig(level=logging.INFO if args.logging == "info"
                    else logging.DEBUG)

Kd = args.Kd
Kw = args.Kw
Kt = args.Kt
dt = args.dt
age_for_active = args.age_for_active
behavior = args.behavior
np.random.seed(args.seed)
fig_ylabel = {'SimplePhototaxis': 'phototaxis\n ',
              'SinePhototaxis': 'sinusoidal\n phototaxis',
              'Photophobia': 'photophobia\n '}[args.behavior]
fig_show_title = False
if args.behavior == 'SimplePhototaxis':
    fig_show_title = True

nsensors = 2
nmotors = 2
radius = 0.25
beta = np.pi / 6.0
relocate_every = 25  # Time
position_range = (-2, 2)
T_train = 100
T_test = args.Ttest

idsm = IDSM(nsensors=nsensors, nmotors=nmotors,
            Kd=Kd, Kw=Kw, Kt=Kt,
            age_for_active=age_for_active)
robot = robots.DiffDriveRobot(nsensors=nsensors,
                              radius=radius)

def norm_sensor(s):
    return s

def denorm_sensor(s):
    return s

def norm_motor(m):
    return 0.5 + 0.5 * m

def denorm_motor(m):
    return (m - 0.5)/0.5

def clip_motor(m):
    # We need to clip the motor velocities
    # to guarantee that when normalized, it stays
    # in [0, 1]
    return np.clip(m, -1.0, 1.0)

def get_norm_sm_state(robot):
    s = robot.get_sensors()
    m = robot.get_motors()
    return np.concatenate([norm_sensor(s), norm_motor(m)])


def get_sensor_values(robot, beta):
    alpha = robot.get_orientation()
    b1 = np.array([np.cos(alpha + beta), np.sin(alpha + beta)])
    b2 = np.array([np.cos(alpha - beta), np.sin(alpha - beta)])
    drange = position_range[1] - position_range[0]
    X, Y = np.meshgrid([position_range[0]- drange/2.0, 0.0, position_range[0] + drange/2.0],
                                                 [position_range[0]- drange/2.0, 0.0, position_range[0] + drange/2.0])
    light_positions = np.concatenate((X.ravel()[:, np.newaxis], Y.ravel()[:, np.newaxis]), axis=1)
    sensor_responses = []
    for b in (b1, b2):
        sensor_pos = robot.get_position() + robot.radius * b
        # Compute the normalized vector  sensor -> light
        c = light_positions - sensor_pos
        c_norm = np.linalg.norm(c, axis=1)
        b_responses = np.clip(np.sum(b * c, axis=1)/c_norm, 0.0, None)/(1.0 + c_norm**2)
        max_response = b_responses.max()
        sensor_responses.append(max_response)
    return np.array(sensor_responses) 


# The forced motor command
if behavior == 'SimplePhototaxis':
    def motor_targets(robot, t):
        s = robot.get_sensors()
        xi = 1 - 1.5 * np.sqrt(s)
        return np.clip(xi, -1.0, 1.0)
elif behavior == 'SinePhototaxis':
    def motor_targets(robot, t):
        s = robot.get_sensors()
        xi = 1 - 1.5 * np.sqrt(s) + \
                np.array([np.sin(2*t)/2, -np.sin(2*t)/2])
        return np.clip(xi, -1.0, 1.0)
else:
    def motor_targets(robot, t):
        s = robot.get_sensors()
        xi = 1 - 1.5 * np.sqrt(np.array([s[1], s[0]]))
        return np.clip(xi, -1.0, 1.0)


def integrate_motor_command(motor_targets, robot, t, dt):
    motor_velocities = robot.get_motors()
    return motor_velocities + dt * (motor_targets(robot, t) - motor_velocities)


def idsm_motor_command(idsm, robot, t, dt):
    '''
    Return the motor commmand given by the idsm
    '''
    global mu
    mu += dt * idsm.velocity_attraction(get_norm_sm_state(robot))
    mu = np.clip(mu, 0.0, 1.0)
    return denorm_motor(mu)


def randomize_robot(robot):
    global mu
    motor_state = np.random.random((nmotors,))
    robot.position = position_range[0] + \
            (position_range[1] - position_range[0]) * \
            np.random.random((2,))
    robot.orientation = -np.pi + 2.0 * np.pi * np.random.random()
    robot.set_motors(denorm_motor(motor_state))
    robot.set_sensors(get_sensor_values(robot, beta))
    # Start with the mu equals to the new random motor state.
    mu = motor_state
    # Start with mu corresponding to a null velocity
    # mu = 0.5 * np.ones_like(motor_state)


def episode(T0, duration, robot, idsm, dt, history, history_reloc,
            fmotor_command,
            movie):
    '''
    Runs an episode of duration num_iter * dt
    Arguments:
        T0: the starting time, used for registering histories
        duration: the duration of the episode
        robot: the robot we use, derives from robot.BaseRobot
        idm: an IDSM object
        dt : the time step for the simulated physics
        history : an utils.RobotHistory for collecting histories of
            position, velocity, commands, ...
        history_reloc : an utils.RobotHistory for collecting the positions
                        just before relocating the robot
        num_iter (int) : the number of iterations to simulate
        fmotor_command (func: (smstate, t) -> ndarray)
    '''
    global mu, relocate_every, position_range
    t = T0
    period = 1
    nb_period = int(duration / relocate_every)
    # a ProgressBar, do not forget pbar.update() and pbar.finish()
    pbar = ProgressBar(maxval=duration + dt).start()
    while t < T0 + duration:

        # Reset the position if necessary
        if t >= T0 + period * relocate_every:
            logging.info("Relocating the robot at {}, period {}/{}".format(t, period, nb_period))
            logging.info("The IDSM has {} nodes".format(len(idsm.nodes)))
            pbar.update(t - T0)
            # Memorize the current location of the robot just before relocation
            # to a random position
            history_reloc.record(t)
            history.record(t)
            history.record_break()
            randomize_robot(robot)
            idsm.reset_dsm()
            period += 1

        # Compute the new motor values
        # motor_state = robot.get_motors()
        motor_state = fmotor_command(robot, t, dt)
        logging.debug("Motor state : {}".format(motor_state))
        motor_state = clip_motor(motor_state)
        robot.set_motors(motor_state)

        # Move the robot
        robot.step(dt)

        # Update the sensors
        robot.set_sensors(get_sensor_values(robot, beta))

        # Update the IDSM
        logging.debug("Sensor state : {}".format(robot.get_sensors()))
        logging.debug("Motor state : {}".format(robot.get_motors()))
        logging.debug("SM state : {}".format(get_norm_sm_state(robot)))
        idsm.step(dt, get_norm_sm_state(robot))

        # Clip its position if necessary
        # This is done after the IDSM update to avoid overwise errors
        # in the computation of the velocity vectors
        # There are some computations below that takes care of the cyclic
        # boundary conditions so that we get nice displays later on when
        # the robot is crossing a boundary
        position = robot.get_position()
        if np.any(position > position_range[1]) or \
           np.any(position < position_range[0]):
            coef_x = 0
            coef_y = 0
            if position[0] < position_range[0]:
                coef_x = -1
            elif position[0] > position_range[1]:
                coef_x = 1
            if position[1] < position_range[0]:
                coef_y = -1
            elif position[1] > position_range[1]:
                coef_y = 1
            # Determine the position on the boundary of the arena
            clip_position = position.copy()
            clip_position[0] = np.clip(position[0],
                                       position_range[0],
                                       position_range[1])
            clip_position[1] = np.clip(position[1],
                                       position_range[0],
                                       position_range[1])
            robot.set_position(clip_position)
            # Record the position before leaving the boundary
            history.record(t)
            history.record_break()
            # Place the robot on the boundary it crossed
            clip_position[0] = clip_position[0] - coef_x * (position_range[1] - position_range[0])
            clip_position[1] = clip_position[1] - coef_y * (position_range[1] - position_range[0])
            robot.set_position(clip_position)
            history.record(t)
            # Use cyclic boundary conditions
            position = np.remainder(position - position_range[0],
                                    position_range[1] - position_range[0]) + \
                    position_range[0]
            robot.set_position(position)
            idsm.reset_dsm()

        # Record the state of the robot
        history.record(t)
        if movie:
            movie.record(t)

        # Let us display the number of nodes, active nodes, ..
        idsm.debug()

        t += dt
        logging.debug("t = {}".format(t))
        pbar.update(t - T0)

    pbar.finish()
    return t


# Let's go for a training and testing
history_robot = RobotHistory(robot)
history_reloc = RobotHistory(robot)
# Create the movie object
if args.movie:
    movie = Movie(bounds=(position_range, position_range),
                  robot_history=history_robot,
                  filename=f'exp-003-{args.behavior}.mp4',
                  fps=15, every=30, horizon=1000)
else:
    movie = None

t0 = 0.0
randomize_robot(robot)
tend = episode(t0, T_train, robot, idsm, dt,
               history_robot, history_reloc,
               fmotor_command=functools.partial(integrate_motor_command,
                                                motor_targets),
               movie=movie)
history_robot.record_break()
randomize_robot(robot)
tend = episode(tend, T_test, robot, idsm, dt,
               history_robot, history_reloc,
               fmotor_command=functools.partial(idsm_motor_command,
                                                idsm),
               movie=movie)
if args.movie:
    movie.save()

# Make the plots
logging.info("Generating the plots")
# We split the history into subplots with 250 timeunits
Tplots = 250
pbar = ProgressBar(maxval=6 + int(np.ceil(tend / Tplots))).start()

fig = plt.figure(figsize=(30,5))
ax = plt.subplot(211)
history_robot.plot_motor_history(ax, '--k')
history_robot.plot_sensor_history(ax, '--b')
ax.set_ylabel("Denormalized SM state")
ax.set_xlabel("Time")
ax.set_aspect('auto')
pbar.step()

ts, _ = history_robot.break_serie(history_robot.times, Padding.CONSTANT)
positions = np.array(history_robot.break_serie(history_robot.position_history,
                                               Padding.NAN)[0])
ax = plt.subplot(212)
ax.plot(ts, positions[:, 0], label='x')
ax.plot(ts, positions[:, 1], label='y')
ax.set_ylabel("Position")
ax.set_xlabel("Time")
ax.set_aspect('auto')
ax.legend()
pbar.step()

fig.tight_layout()
plt.savefig('figs/figure7_{}_debug.pdf'.format(args.behavior),
            bbox_inches='tight')
pbar.step()

# We split the history into subplots with Tplots timeunits
num_plots = int(np.ceil(tend / Tplots))
mean_sq_distances = []
history_gen = history_robot.split_position_history(Tplots)
pbar.step()

fig = plt.figure(figsize=(10, 1))
gs = gridspec.GridSpec(1, num_plots)
gs.update(wspace=0.075, hspace=0.05)
axfirst = None

for iax, (gsi, (timesi, positionsi)) in enumerate(zip(gs, history_gen)):
    if axfirst is None:
        ax = fig.add_subplot(gsi)
        axfirst = ax
    else:
        ax = fig.add_subplot(gsi, sharey=axfirst)
        plt.setp(ax.get_yticklabels(), visible=False)
    tmin = timesi[0]
    tmax = timesi[-1]

    nppositions = np.array(positionsi)
    ax.plot(nppositions[:, 0], nppositions[:, 1], 'k-', linewidth=0.5)

    logging.info("{}".format(np.array(history_reloc.times)))
    reloc_idx = np.logical_and(np.array(history_reloc.times) >= tmin,
                               np.array(history_reloc.times) < tmax)
    ax.scatter(np.array(history_reloc.position_history)[reloc_idx, 0],
               np.array(history_reloc.position_history)[reloc_idx, 1],
               marker='o', s=3**2, edgecolors='k', facecolors='r', zorder=3)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_xlim(position_range)
    ax.set_ylim(position_range)
    ax.set_aspect('equal')
    if iax == 0:
        ax.set_ylabel(fig_ylabel)
    if fig_show_title:
        ax.set_title("{} - {}".format(int(tmin), int(tmax)), size='x-small')
        # plt.setp(ax, title="{} - {}".format(int(tmin), int(tmax)))
    notnan_idx = np.logical_not(np.isnan(nppositions[:, 0]))
    mean_sq_dist = np.mean((nppositions[notnan_idx, :]**2).sum(axis=1))
    logging.info("For time period {} - {}: {}".format(tmin, tmax, mean_sq_dist))
    mean_sq_distances.append(mean_sq_dist)
    pbar.step()
    
# fig.tight_layout()
plt.savefig('figs/figure7_{}.pdf'.format(args.behavior), bbox_inches='tight')
pbar.step()
fname = 'mean_sq_distances_{}.data'.format(args.behavior)
f = open(fname, 'w')
f.write("\n".join(map(str, mean_sq_distances)))
f.close()
logging.info("{} saved".format(fname))
pbar.step()

pbar.finish()
plt.show()
