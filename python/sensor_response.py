#!/usr/bin/env python3

'''
Plot the response of one of the sensors when a light is moving along a line
aligned with the robot
Which is the same as placing a light at 0, 0 and moving the robot from
 -d to +d always facing positive x
'''

# External modules
import numpy as np
import matplotlib.pyplot as plt
# Local modules
import robots

radius = 0.25
beta_0 = np.pi/3.0
beta_1 = np.pi/6.0
x = np.linspace(-15, 5, num=500)

def get_sensor_values(robot, beta):
    alpha = robot.get_orientation()
    b = np.array([[np.cos(alpha + beta), np.sin(alpha + beta)],
                  [np.cos(alpha - beta), np.sin(alpha - beta)]])
    light_pos = np.array([0.0, 0.0])
    sensor_pos = robot.get_position() + robot.radius * b
    # Compute the normalized vector  sensor -> light
    c = light_pos - sensor_pos
    c_norm = np.linalg.norm(c, axis=1)
    return np.clip(np.sum(b * c, axis=1)/c_norm, 0.0, None)/(1.0 + c_norm**2)

def f_motor(sensors):
    return 1 - 1.5 * np.sqrt(sensors)

robot = robots.DiffDriveRobot(nsensors=2,
                              radius=radius)

responses_beta0 = []
responses_beta1 = []
for xi in x:
    robot.set_position(np.array([xi, 0]))
    robot.set_orientation(0.0)
    responses_beta0.append(get_sensor_values(robot, beta_0))
    responses_beta1.append(get_sensor_values(robot, beta_1))

responses_beta0 = np.array(responses_beta0)
responses_beta1 = np.array(responses_beta1)

motorl_beta0 = f_motor(responses_beta0[:, 0])
motorl_beta1 = f_motor(responses_beta1[:, 0])

fig, axs = plt.subplots(1, 1)
axs.plot(x, motorl_beta0, 'k', label=r'$\beta$')
axs.plot(x, motorl_beta1,'k--', label=r'$\pi/6$')
plt.hlines(0.0, x.min(), x.max(), colors='r')
axs.set_xlabel("Position")
axs.set_ylabel(r"Target velocity $\chi$ of simple phototaxis")
plt.legend()

fig.tight_layout()
plt.savefig("figs/Sensor_velocity.pdf", bbox_inches='tight')

plt.show()
