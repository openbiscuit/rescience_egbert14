#!/usr/bin/env python3
# coding: utf-8
'''
Reproduction of Figure 4 of
    Modeling habits as self-sustaining patterns of sensorimotor behavior
    M.D. Egbert and X.E. Barandiaran (2014)
'''

# Standard modules
import argparse
import logging
# External modules
import matplotlib.pyplot as plt
import numpy as np
# Local modules
import robots
from history import RobotHistory
from IDSM import IDSM
import utils
from progress import ProgressBar
from movie import Movie

parser = argparse.ArgumentParser()
parser.add_argument('--dt',
                    type=float,
                    help='The time step for Euler integration',
                    default=0.01)
parser.add_argument('--Kd',
                    type=float,
                    help='Kd for setting the influence radius of a node',
                    default=1000)
parser.add_argument('--Kw',
                    type=float,
                    help='Kw constant for the weight factor function',
                    default=0.0025)
parser.add_argument('--Kt',
                    type=float,
                    help='Kt, the threshold on the density before creating'
                         ' a node',
                    default=1.0)
parser.add_argument('--age_for_active',
                    type=float,
                    help='The time before a node influences the motor output',
                    default=10)
parser.add_argument('--logging', choices=["info", "debug"],
                    help='Which logging level to use',
                    default="info")
parser.add_argument('-m', '--movie', action="store_true",
                    default=False,
                    help='Whether to generate a movie or not (it takes time!)')
parser.add_argument('-v', '--verbose', action="store_true",
                    help='Print verbose information')
args = parser.parse_args()

logging.basicConfig(level=logging.INFO if args.logging == "info"
                    else logging.DEBUG)

Kd = args.Kd
Kw = args.Kw
Kt = args.Kt
dt = args.dt
age_for_active = args.age_for_active

idsm = IDSM(nsensors=0, nmotors=2,
            Kd=Kd, Kw=Kw, Kt=Kt,
            age_for_active=age_for_active)
robot = robots.OmniRobot(nsensors=0, nmotors=2)

def norm_sm_state(m):
    return (1.0 + m)/2.0

def denorm_motor(m):
    return 2.0 * m - 1.0

def get_norm_sm_state(robot):
    m = robot.get_motors()
    return norm_sm_state(m)

# The forced motor command
motor_command = lambda t: np.array([0.75 * np.cos(2.0*np.pi/10.0 * t),
                                    0.75 * np.sin(2.0*np.pi/10.0 * t)])

# The initial robot position
robot.set_motors(np.zeros(2))
robot.set_position(np.zeros(2))

# The object to collect the history of motors, sensors, positions, ..
history = RobotHistory(robot)

# Create the movie object 
if args.movie:
    movie = Movie(bounds=([-2, 2], [-2, 3]),
                  robot_history=history,
                  filename='exp-001.mp4',
                  every=10)

logging.info("Running the simulation with external control")
t = 0.0
# a ProgressBar, do not forget pbar.update() and pbar.finish()
pbar = ProgressBar(maxval=50.0+dt).start()

while t < 20:
    # Drive the robot with the externally
    # provided motor commands
    robot.set_motors(motor_command(t))

    # Move the robot
    robot.step(dt)

    # Update the IDSM
    idsm.step(dt, get_norm_sm_state(robot))

    # Record the state of the robot
    history.record(t)

    # Let us display the number of nodes, active nodes, ..
    idsm.debug()

    # Record the movie frame
    if args.movie:
        movie.record(t)

    t += dt

    logging.debug("t = {}".format(t))
    pbar.update(t)
# Steam plot
# Compute the velocity/attraction vector field
logging.debug("Computing the steam plot")
_, _, velocity, _ = utils.compute_2motors_steam(idsm,
                                                (0.0, 1.0),
                                                (0.0, 1.0),
                                                'velocity_term', num=30)
_, _, attraction, _ = utils.compute_2motors_steam(idsm,
                                                  (0.0, 1.0),
                                                  (0.0, 1.0),
                                                  'attraction_term', num=30)
m1, m2, velocity_attraction, weights = utils.compute_2motors_steam(idsm,
                                                                   (0.0, 1.0),
                                                                   (0.0, 1.0),
                                                                   'velocity_attraction',
                                                                   num=30)

# Random initial condition
# Mu is in normalized coordinates
mu = np.array([0.5, 0.6])
idsm.reset_dsm()
history_restart = RobotHistory(robot)
if args.movie:
    movie.add_history(history_restart)

logging.info("Running from a random initial condition {} with IDSM control".format(mu))
while(t < 50):
    # Drive the robot with the IDSM motor command
    mu += dt * idsm.velocity_attraction(get_norm_sm_state(robot))
    robot.set_motors(denorm_motor(mu))
    if args.verbose:
        print(mu)

    # Move the robot
    robot.step(dt)

    # Update the IDSM
    idsm.step(dt, get_norm_sm_state(robot))

    # Record the state of the robot
    history_restart.record(t)

    # Let us display the number of nodes, active nodes, ..
    idsm.debug()

    # Record the movie frame
    if args.movie:
        movie.record(t)

    t += dt

    logging.debug("t = {}".format(t))
    pbar.update(t)
    
pbar.finish()

# Save the movie
if args.movie:
    movie.save()

# Compute the normalized motor values from the real motors values
np_history = norm_sm_state(np.array(history.motors_history))
np_history_restart = norm_sm_state(np.array(history_restart.motors_history))

# Make the steamplot
logging.info("Generating the plots")
pbar = ProgressBar(maxval=15).start()

# Disable logging, otherwise matplotlib generates a lot
logging.disable(logging.DEBUG)

fig, axes = plt.subplots(figsize=(15, 5), nrows=1, ncols=3, sharey=True)
axes[0].streamplot(m1, m2,
                   velocity[:, :, 0],
                   velocity[:, :, 1],
                   color='k', linewidth=(.25 + weights), density=0.75)
pbar.step()
axes[0].plot(np_history[:, 0],
             np_history[:, 1], c='b')
pbar.step()
axes[0].set_title('Velocity')
axes[0].set_xlim(0, 1)
axes[0].set_ylim(0, 1)

axes[1].streamplot(m1, m2,
                   attraction[:, :, 0],
                   attraction[:, :, 1],
                   color='k', linewidth=(.25 + weights), density=0.75)
pbar.step()
axes[1].plot(np_history[:, 0],
             np_history[:, 1], c='b')
pbar.step()
axes[1].set_title('Attraction')
axes[1].set_xlim(0, 1)
axes[1].set_ylim(0, 1)

axes[2].streamplot(m1, m2,
                   velocity_attraction[:, :, 0],
                   velocity_attraction[:, :, 1],
                   color='k', linewidth=(.25 + weights), density=0.75)
pbar.step()
# Show the trajectory in SM space after restarting in blue
axes[2].plot(np_history_restart[:, 0],
             np_history_restart[:, 1], c='b')
pbar.step()
# Show the starting position with a star
axes[2].plot(np_history_restart[0, 0],
             np_history_restart[0, 1], 'k',
             markersize=20, marker='*',
             fillstyle='none',
             markerfacecolor='w',
             markeredgecolor='k',
             markeredgewidth=2.0)
pbar.step()
axes[2].set_title('Combined')
axes[2].set_xlim(0, 1)
axes[2].set_ylim(0, 1)
for ax in axes.flat:
    ax.set(xlabel='motor 1', ylabel='motor 2')
for ax in axes.flat:
    ax.label_outer()

pbar.step()
plt.savefig('figs/figure4.pdf', bbox_inches='tight')
logging.info("figs/figure4.pdf saved")

#####
#
fig = plt.figure()
ax = plt.subplot(211)
history.plot_motor_history(ax)
pbar.step()
history_restart.plot_motor_history(ax)
pbar.step()
ax.set_xlabel('Time')
ax.set_ylabel('Motor values')
ax.set_title("Normalized motor values")

ax = plt.subplot(223)
history.plot_position_history(ax)
pbar.step()
history_restart.plot_position_history(ax)
pbar.step()
ax.set_aspect('equal')
ax.set_xlabel('x')
ax.set_xlabel('y')
ax.set_title("Position")

ax = plt.subplot(224)
ax.scatter(idsm.nodes['position'][:, 0],
           idsm.nodes['position'][:, 1],
           c=idsm.weights(), cmap='gray')
pbar.step()
# The position of the nodes with their velocity vectors
for p, v in zip(idsm.nodes['position'],
                idsm.nodes['velocity']):
    ax.plot([p[0], p[0]+v[0]],
            [p[1], p[1]+v[1]], 'k')
pbar.step()
# The trajectory in SM space after restarting
ax.plot(np_history_restart[:, 0],
        np_history_restart[:, 1],
        c='r')
pbar.step()
ax.set_xlabel('motor1')
ax.set_ylabel('motor2')
ax.set_title('IDSM nodes/vel vectors')
ax.set_xlim(0, 1)
ax.set_ylim(0, 1)
ax.set_aspect('equal')

fig.tight_layout()

pbar.finish()
plt.show()
