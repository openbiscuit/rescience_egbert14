#!/usr/bin/env python3
'''
Reproduction of Figures 1, 2, 3 of
    Modeling habits as self-sustaining patterns of sensorimotor behavior
    M.D. Egbert and X.E. Barandiaran (2014)
Notes:
    For figure 3, the weight has been multiplied by 10 to get a visually
    similar effect than in the original paper
'''

# Standard modules
import argparse
# External modules
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
# Local modules
import IDSM
import utils

parser = argparse.ArgumentParser()
parser.add_argument('--Kd',
                    type=float,
                    help='Kd for setting the influence radius of a node',
                    default=1000)
parser.add_argument('--Kw',
                    type=float,
                    help='Kw constant for the weight factor function',
                    default=0.0025)
parser.add_argument('--Nv',
                    type=float,
                    help='Nv values for the 4 node experiment',
                    default=1.0)
args = parser.parse_args()

Kw = args.Kw
Kd = args.Kd

distance_factor = lambda x: 2.0 / (1.0 + np.exp(Kd * x**2))
weight_factor = lambda x: 2.0 / (1.0 + np.exp(-Kw * x))

# Figure 1
x_distances = np.linspace(0.0, 0.15)
distances = distance_factor(x_distances)

x_weights = np.linspace(-2000, 2000)
weights = weight_factor(x_weights)

plt.figure(figsize=(10, 5))
plt.subplot(121)
plt.plot(x_distances, distances, 'k', linewidth=2)
plt.xlabel(r'$||N_p -x||$ (distance to node)')
plt.ylabel(r'$d(N_p, x)$')
plt.title('Distance factor')

plt.subplot(122)
plt.plot(x_weights, weights, 'k', linewidth=2)
plt.xlabel(r'$N_w$ (weight)')
plt.ylabel(r'$\omega(N_w)$')
plt.title('Weight factor')

plt.savefig('figs/figure1.pdf', bbox_inches='tight')

# Figure 2
# Build a hypothetical 2-Motor 0-Sensor IDSM
idsm = IDSM.IDSM(nsensors=0, nmotors=2,
                 Kd=Kd, Kw=Kw, Kt=1.0,
                 age_for_active=-1)

# Add a single node at Np=(0.5, 0.5), Nv=(0.0, 0.1)
idsm.create_node(np.array([0.5, 0.5]), np.array([0.0, 0.1]))

fig, axes = plt.subplots(figsize=(15, 5), nrows=1, ncols=3, sharey=True)

m1, m2, influence, weights = utils.compute_2motors_steam(idsm,
                                                         (0.35, 0.65),
                                                         (0.35, 0.65),
                                                         'velocity_term',
                                                         num=25)
axes[0].streamplot(m1, m2,
                   influence[:, :, 0], influence[:, :, 1],
                   color='k', linewidth=(.25 + 2.0*weights))
axes[0].set_title('Velocity')
m1, m2, influence, weights = utils.compute_2motors_steam(idsm,
                                                         (0.35, 0.65),
                                                         (0.35, 0.65),
                                                         'attraction_term',
                                                         num=25)
axes[1].streamplot(m1, m2,
                   influence[:, :, 0], influence[:, :, 1],
                   color='k', linewidth=(.25 + 2.0*weights))
axes[1].set_title('Attraction')
m1, m2, influence, weights = utils.compute_2motors_steam(idsm,
                                                         (0.35, 0.65),
                                                         (0.35, 0.65),
                                                         'velocity_attraction',
                                                         num=25)
axes[2].streamplot(m1, m2,
                   influence[:, :, 0], influence[:, :, 1],
                   color='k', linewidth=(.25 + 2.0*weights))
axes[2].set_title('Velocity and attraction')

for ax in axes.flat:
    ax.set_xlim(0.35, 0.65)
    ax.set_ylim(0.35, 0.65)
    ax.set(xlabel='motor 1', ylabel='motor 2')
for ax in axes.flat:
    ax.label_outer()

plt.savefig('figs/figure2.pdf', bbox_inches='tight')

# Figure 3
# Build a hypothetical 2-Motor 0-Sensor IDSM
idsm = IDSM.IDSM(nsensors=0, nmotors=2,
                 Kd=Kd, Kw=Kw, Kt=1.0,
                 age_for_active=-1)

# Add the four nodes
v = args.Nv
idsm.create_node(np.array([0.55, 0.5]), np.array([0.0, -v]))
idsm.create_node(np.array([0.5, 0.45]), np.array([-v, 0.0]))
idsm.create_node(np.array([0.45, 0.5]), np.array([0.0, v]))
idsm.create_node(np.array([0.5, 0.55]), np.array([v, 0.0]))

fig, axes = plt.subplots(figsize=(10, 2), nrows=1, ncols=5, sharey=True)
for i, (wi, ax) in enumerate(zip([-500, -100, 0, 50, 100], axes.flat)):
    idsm.nodes['weight'][0] = wi
    m1, m2, influence, weights = utils.compute_2motors_steam(idsm,
                                                             (0.4, 0.6),
                                                             (0.4, 0.6),
                                                             'velocity_attraction',
                                                             num=25)

    ax.streamplot(m1, m2,
                  influence[:, :, 0],
                  influence[:, :, 1],
                  density=0.75,
                  color='k', linewidth=weights)
    ax.set_title(r'$N_w = {}$'.format(wi))

for ax in axes.flat:
    ax.set_xlim(0.4, 0.6)
    ax.set_ylim(0.4, 0.6)
    ax.set_aspect('equal')
    # Draw the circle. Matplotlib requires a different artist instance per axis
    circle = matplotlib.patches.Circle((0.55, 0.5), 0.025,
                                       edgecolor='k',
                                       fill=False)
    ax.add_artist(circle)
    for n in idsm.nodes:
        pos = n['position']
        vel = n['velocity']
        width = 0.005
        head_length = 3.0 * width
        dx = vel[0] / np.sqrt(vel[0]**2 + vel[1]**2)
        dy = vel[1] / np.sqrt(vel[0]**2 + vel[1]**2)
        tot_length = 0.03
        tail_length = tot_length - head_length
        ax.arrow(x=pos[0]-tot_length/2*dx, y=pos[1]-tot_length/2*dy,
                 dx=tail_length*dx, dy=tail_length*dy, width=width,
                 head_length=head_length, edgecolor='k',
                 facecolor='k')

plt.tight_layout()
plt.savefig('figs/figure3.pdf', bbox_inches='tight')

plt.show()
