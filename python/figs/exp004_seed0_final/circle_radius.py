#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# External modules
import numpy as np
import matplotlib.pyplot as plt

diam = { 
    1: 1.1,
    2: 0.2,
    3: 0.2,
    4: 0.2,
    5: 1.0,
    6: 0.2,
    7: 0.15,
    8: 0.85,
    9: 1.2,
    10: 0.2,
    11: 0.17,
    12: 0.17,
    13: 0.18,
    14: 1.17,
    15: 0.18,
    16: 0.2,
    17: 0.22,
    18: 0.22,
    19: 0.18,
    20: 1.1,
    21: 0.22,
    22: 0.23,
    23: 0.21,
    24: 0.21,
    25: 0.22,
    26: 0.22,
    27: 1.2,
    28: 0.2,
    29: 0.17,
    30: 0.14,
    31: 0.2,
    32: 0.16,
    33: 0.18,
    34: 1.1,
    35: 0.2,
    36: 0.2,
    37: 1.1,
    38: 0.21,
    39: 0.15,
    40: 0.25,
    41: 1.05,
    42: 0.81,
    43: 1.17,
    44: 1.15,
    45: 0.16,
    46: 0.75,
    47: 1.0,
    48: 1.1,
    49: 0.2,
    50: 0.26,
    51: 0.26,
    52: 0.17,
    53: 0.95,
    54: 0.22,
    55: 0.22,
    56: 1.15,
    57: 1.05,
    58: 0.22,
    59: 0.22,
    60: 0.95,
    61: 0.22, 
    62: 1.21,
    63: 0.25,
    64: 0.23,
    65: 1.2,
    66: 0.25,
    67: 0.62, 
    68: 0.23,
    69: 0.8,
    70: 0.2,
    71: 0.18,
    72: 1.15,
    73: 1.2,
    74: 0.25, 
    75: 0.25,
    76: 1.05,
    77: 1.17,
    78: 0.7,
    79: 0.7,
    80: 0.18,
    81: 0.19,
    82: 1.1,
    83: 0.23,
    84: 0.16,
    85: 0.16,
    86: 0.6,
    87: 0.8,
    88: 0.2,
    89: 0.16,
    90: 0.19
}

maxval = 1.3

x = np.array( list(diam.values()) )
y = np.random.default_rng().normal( 0, 0.1, len(diam)) -1
##y = np.zeros_like( x )
fig, ax = plt.subplots(figsize=(14, 3))
##pts = ax.plot( x, y, 'x' )
##pts = ax.plot( x, y, 0.7)
pts = ax.scatter( x, y, s=5.0 )
ax.set_xlabel( "Diameters" )
##ax.set_ylim(-1, 1)
##ax.axes.yaxis.set_ticks([])
bins = np.arange(0, maxval, 0.1)

print(f'Hidden radius : {sorted([p for p in diam.values() if p > maxval])} out of {len(diam.values())}')

bar = ax.hist( x, bins, color='k',rwidth=0.8)
plt.xlim([0, maxval])
# ax.set_xlabel("Diameter intervale")
ax.set_ylabel("Number of instances")

fig.tight_layout()

plt.savefig("figure9_dist.pdf", bbox_inches="tight")

plt.show()
