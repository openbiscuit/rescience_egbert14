#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# External modules
import numpy as np
import matplotlib.pyplot as plt

diam = { 
    1: 1.4,
    2: 1.4,
    3: 0.3,
    4: 0.3,
    5: 0.08,
    6: 0.1,
    7: 0.3,
    8: 1.4,
    9: 0.1,
    10: 0.1,
    11: 0.3,
    12: 0.1,
    13: 0.1,
    14: 1.0,
    15: 0.3,
    16: 0.3,
    17: 0.32,
    18: 0.1,
    19: 0.3,
    20: 1,
    21: 0.08,
    22: 1,
    23: 0.3,
    24: 0.9,
    25: 0.3,
    26: 0.1,
    27: 0.12,
    28: 1.0,
    29: 0.08,
    30: 0.1,
    31: 1.0,
    32: 0.3,
    33: 1.75,
    34: 0.07,
    35: 0.14,
    36: 0.06,
    37: 1.75,
    38: 0.33,
    39: 0.08,
    40: 0.13,
    41: 1.1,
    42: 0.04,
    43: 0.65,
    44: 1.0,
    45: 0.3,
    46: 0.08,
    47: 0.12,
    48: 1.0,
    '49a': 2000, '49b': 2, '49c': 2,
    50: 0.08,
    '51a': 90, '51b': 20, '51c': 25,
    52: 0.08,
    53: 0.12,
    54: 1.75,
    55: 0.08,
    56: 0.08,
    57: 0.08,
    58: 0.7,
    '59a':375, '59b': 40, '59c': 25,
    60: 0.08,
    '61a': 4.2, '61b': 0.7, '61c': 0.5,
    62: 0.65,
    63: 0.08,
    64: 1.75,
    65: 0.65,
    66: 1.5,
    67: 0.04, 
    68: 0.08,
    69: 0.08,
    70: 0.08,
    71: 1.75,
    73: 1.0,
    '74a': 35, '74b': 12, '74c': 11,
    75: 0.08,
    76: 1.0,
    77: 3.0,
    78: 0.08,
    79: 0.08,
    80: 1.75,
    '81a': 5.0, '81b': 0.75, '81c': 0.5,
    82: 1.0,
    '83a': 1.6, '83b': 1.8, '83c': 0.5,
    '84a': 14000, '84b': 13.12, '84c': 18.75,
    85: 1.0,
    86: 0.65,
    87: 1.6,
    89: 0.08,
    '90a': 7,'90b': 3.2,'90c': 1.75
}

maxval = 8 

x = np.array( list(diam.values()) )
y = np.random.default_rng().normal( 0, 0.1, len(diam)) -1
##y = np.zeros_like( x )
fig, ax = plt.subplots(figsize=(14, 3))
##pts = ax.plot( x, y, 'x' )
##pts = ax.plot( x, y, 0.7)
pts = ax.scatter( x, y, s=5.0 )
ax.set_xlabel( "Diameters" )
##ax.set_ylim(-1, 1)
##ax.axes.yaxis.set_ticks([])
bins = np.arange(0, maxval, 0.1)

print(f'Hidden radius : {sorted([p for p in diam.values() if p > maxval])} out of {len(diam.values())}')

bar = ax.hist( x, bins, color='k',rwidth=0.8)
plt.xlim([0, maxval])
# ax.set_xlabel("Diameter intervale")
ax.set_ylabel("Number of instances")

fig.tight_layout()

plt.savefig("figure9_dist.pdf", bbox_inches="tight")

plt.show()
