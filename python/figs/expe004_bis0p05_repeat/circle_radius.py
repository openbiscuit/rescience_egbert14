#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# List the approximate diameter  of circle like patterns
# and plot histogram

# External modules
import numpy as np
import matplotlib.pyplot as plt

diam = { 1 : 0.09,
         4 : 0.11,
         7 : 0.8,
         9 : 0.04,
         10 : 0.1,
         11: 0.12,
         12: 0.5,
         13: 0.1,
         14: 0.03,
         '15a': 0.22,
         '15b': 0.21,
         '15c': 0.07,
         16: 0.6,
         17: 0.025,
         '18a': 0.14,
         '18b': 0.06,
         '18c': 0.03,
         '19a': 15,
         '19b': 10,
         '19c': 2.5,
         20: 0.02,
         21: 1.0,
         22: 0.6,
         23: 0.75,
         24: 3.5,
         25: 0.03,
         '26a': 300,
         '26b': 50,
         '26c': 25,
         '26d': 20,
         29: 0.6,
         '31a': 15, '31b':10, '31c': 8,
         32: 3.0,
         33: 0.6,
         34: 0.04,
         '35a': 5,'35b': 2.5, '35c': 1.5,
         '36a': 4.0, '36b': 2.8,
         '37a': 0.02, '37b': 0.01,
         39: 0.05,
         42: 0.01,
         43: 4,
         '44a': 1.3, '44b': 0.5, '44c': 0.3,
         45: 0.05,
         46: 0.85,
         47: 0.025,
         '48a':7.5, '48b':3,
         49: 6,
         50: 0.04,
         53: 0.025,
         '54a': 6, '54b': 2.8,
         55: 0.02,
         '56a': 17, '56b': 4,
         '58a': 400, '58b': 200,
         '59a': 27, '59b': 2, '59c': 1,
         '60a': 0.22, '60b': 0.3, '60c': 0.26,
         '61a': 53, '61b': 22,
         63: 2.7,
         66: 12.5,
         67: 0.06,
         '68a': 2000, '68b': 1000,
         69: 0.02,
         '71a': 50, '71b': 12, '71c':10,
         72: 0.02,
         73: 0.08,
         '74a':90, '74b': 90,
         '75a': 3200, '75b': 500, '75c':500,
         76: 0.7,
         77: 2.5,
         78: 0.03,
         81: 2.2,
         '83a':0.4, '83b':0.15, '83c': 0.1,
         85: 0.4,
         86: 2.3,
         '87a':1.2, '87b':0.4, '87c': 0.25,
         88: 0.08,
         '89a': 60, '89b': 8, '89c': 3,
}
x = [x for x in diam.values() if x < 1.5]
x = np.array( x )
##x = np.array( list(diam.values()) )
y = np.random.default_rng().normal( 0, 0.1, len(x)) -1
##y = np.zeros_like( x )
fig, ax = plt.subplots(figsize=(14, 3))
##pts = ax.plot( x, y, 'x' )
##pts = ax.plot( x, y, 0.7)
pts = ax.scatter( x, y, s=5.0 )
ax.set_xlabel( "Diameters" )
##ax.set_ylim(-1, 1)
##ax.axes.yaxis.set_ticks([])
bar = ax.hist( x, 20, color='k',rwidth=0.8)
# ax.set_xlabel("Diameter intervale")
ax.set_ylabel("Number of instances")

fig.tight_layout()

plt.savefig("figure9_dist.pdf", bbox_inches="tight")

plt.show()
