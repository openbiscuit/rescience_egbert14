#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# List the approximate diameter  of circle like patterns
# and plot histogram

# External modules
import numpy as np
import matplotlib.pyplot as plt

diam = {
    2: 2,
    4: 0.65,
    5: 0.7,
    7: 0.7,
    8: 0.8,
    9: 0.7,
    10: 0.7,
    11: 0.17,
    12: 2.5,
    13: 1.75,
    14: 0.5,
    15: 0.09,
    16: 1.0,
    17: 0.08,
    18: 0.085,
    19: 0.022,
    21: 0.08,
    22: 0.3,
    23: 1.05,
    25: 1.1,
    '26a': 0.0006, '26b': 0.0002, '26c': 0.01,
    27: 0.09,
    28: 0.25,
    29: 0.6,
    30: 0.1,
    31: 0.01,
    32: 1.1,
    34: 0.09,
    35: 0.25,
    '37a': 45, '37b': 40,
    38: 0.6,
    42: 0.22,
    43: 0.25,
    44: 0.25,
    45: 0.075,
    46: 0.1,
    47: 0.25,
    49: 0.1,
    50: 0.0035,
    51: 0.25,
    '52a': 2.2, '52b': 1.25, '52c': 1,
    53: 1600,
    54: 0.04,
    55: 0.1,
    56: 13,
    57: 0.6,
    58: 0.01,
    59: 0.35,
    61: 0.3,
    63: 0.1,
    64: 0.25,
    65: 0.25,
    66: 0.25,
    67: 0.35,
    68: 0.25,
    71: 0.25,
    72: 0.007,
    73: 0.25,
    74: 0.25,
    75: 0.35,
    76: 0.6,
    78: 0.7,
    79: 0.25,
    80: 0.35,
    81: 0.4,
    '82a': 6, '82b': 3, '82c': 2.6,
    '85a': 7, '85b': 6, '85c': 10,
    86: 0.25,
    '87a': 45, '87b': 3, '87c': 2.5,
    88: 0.1,
    '89a': 21, '89b': 6, '89c': 12.5,
    '90a': 165, '90b': 13, '90c': 17, 
         
}
x = [x for x in diam.values() if x < 1.5]
x = np.array( x )
##x = np.array( list(diam.values()) )
y = np.random.default_rng().normal( 0, 0.1, len(x)) -1
##y = np.zeros_like( x )
fig, ax = plt.subplots(figsize=(14, 3))
##pts = ax.plot( x, y, 'x' )
##pts = ax.plot( x, y, 0.7)
pts = ax.scatter( x, y, s=5.0 )
ax.set_xlabel( "Diameters" )
##ax.set_ylim(-1, 1)
##ax.axes.yaxis.set_ticks([])
bar = ax.hist( x, 20, color='k',rwidth=0.8)
# ax.set_xlabel("Diameter intervale")
ax.set_ylabel("Number of instances")

fig.tight_layout()

plt.savefig("figure9_dist.pdf", bbox_inches="tight")

plt.show()
