#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# External modules
import numpy as np
import matplotlib.pyplot as plt

diam = { 1: 0.4,
           3: 3.5,
           4: 0.6,
           5: 1.0,
           6: 0.2,
           7: 0.08,
           8: 1.75,
           9: 0.8,
           10: 10,
           13: 0.6,
           14: 0.6,
           15: 2.2,
           16: 1.5,
           18: 2.5,
           19: 7,
           20: 0.35,
           21: 3,
           22: 0.9,
           23: 0.9,
           24: 0.35,
           25: 0.3,
           26: 1.8,
           27: 0.85,
           28: 0.2,
           29: 0.15,
           30: 1.9,
           31: 0.3,
           33: 0.65,
           34: 0.15,
           36: 2,
           37: 0.7,
           38: 1,
           39: 1.75,
           40: 0.8,
           41: 0.05,
           42: 1,
           43: 2.5,
           44: 0.6,
           47: 0.65,
           48: 1.2,
           49: 1,
           51: 1.2,
           52: 3,
           53: 2.2,
           54: 0.8,
           56: 0.65,
           57: 1,
           58: 0.2,
           59: 0.65,
           60: 4,
           61: 0.3,
           62: 2,
           64: 1.8,
           65: 0.6,
           66: 3,
           67: 1.8,
           69: 6,
           71: 0.3,
           72: 1.75,
           73: 2.5,
           74: 0.1,
           75: 0.25,
           76: 2,
           77: 3.5,
           78: 0.4,
           79: 1.2,
           80: 1.25,
           81: 0.3,
           82: 6,
           83: 3,
           84: 0.3,
           85: 1.4,
           86: 1.6,
           87: 1.15,
           88: 2.7,
           89: 0.26,
           90: 0.5 }

x = np.array( list(diam.values()) )
y = np.random.default_rng().normal( 0, 0.1, len(diam)) -1
##y = np.zeros_like( x )
fig, ax = plt.subplots(figsize=(14, 3))
##pts = ax.plot( x, y, 'x' )
##pts = ax.plot( x, y, 0.7)
pts = ax.scatter( x, y, s=5.0 )
ax.set_xlabel( "Diameters" )
##ax.set_ylim(-1, 1)
##ax.axes.yaxis.set_ticks([])
bar = ax.hist( x, 20, color='k',rwidth=0.8)
# ax.set_xlabel("Diameter intervale")
ax.set_ylabel("Number of instances")

fig.tight_layout()

plt.savefig("figure9_dist.pdf", bbox_inches="tight")

plt.show()
