import numpy as np

class BaseRobot:

    """
    A Robot is just a wrapper around :
        - a collection of motor states (np.array)
        - a collection of sensor states (np.array)
    """

    def __init__(self, nsensors, nmotors):
        self.nmotors = nmotors
        self.nsensors = nsensors

        self.motors = np.zeros(nmotors)
        self.sensors = np.zeros(nsensors)

    def get_motors(self):
        return self.motors.copy()

    def set_motors(self, motor_speed):
        self.motors = motor_speed

    def get_sensors(self):
        return self.sensors.copy()

    def set_sensors(self, sensors):
        self.sensors = sensors

class DiffDriveRobot(BaseRobot):
    """
    A DiffDriveRobot
    """
    def __init__(self, nsensors, radius):
        super(DiffDriveRobot, self).__init__(nsensors, 2)
        self.position = -1.0 + 2.0 * np.random.random(2)
        self.orientation = -np.pi + 2.0 * np.pi * np.random.random()
        self.radius = radius

    def get_position(self):
        return self.position.copy()

    def set_position(self, position):
        self.position = position

    def get_orientation(self):
        return self.orientation

    def set_orientation(self, orientation):
        self.orientation = orientation

    def step(self, dt):

        self.position += dt * self.motors.sum() * np.array([np.cos(self.orientation), np.sin(self.orientation)])
        self.orientation += dt * (self.motors[1] - self.motors[0]) / (2.0 * self.radius)
        # Keep the orientation in [-pi, pi]
        self.orientation = (self.orientation + np.pi) % (2.0 * np.pi) - np.pi



class OmniRobot(BaseRobot):

    """
    A OmniRobot is a BaseRobot that can move
    omnidirectionnally in the space of the same dimension
    as the motor space. It has
        - a position updated with the motor state
    """

    def __init__(self, nsensors, nmotors):
        super(OmniRobot, self).__init__(nsensors, nmotors)
        self.position = np.zeros(nmotors)

    def get_position(self):
        return self.position.copy()

    def set_position(self, position):
        self.position = position

    def step(self, dt):
        self.position += dt * self.motors


