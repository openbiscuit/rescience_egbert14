#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Add '-final' to PDF plots in the selected directory
"""

import sys
import os

if len(sys.argv) < 2:
    print("usage: {} dir0 [dir1] ...".format( sys.argv[0] ))
    sys.exit(1)

for dir in sys.argv[1:]:
    print( "Make final PDF in {}".format( dir ) )
    allfiles = [f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir, f))]
    pdffiles = [f for f in allfiles if f.endswith( '.pdf' )]
    ##print( "=>",pdffiles)
    
    for f in pdffiles:
        filename, file_extension = os.path.splitext(f)
        if not filename.endswith( '_final' ):
            print( "rename {} => {}".format( dir+'/'+f,
                                             dir+'/'+filename+'_final.pdf' ))
            os.rename( dir+'/'+f, dir+'/'+filename+'_final.pdf' )
    





