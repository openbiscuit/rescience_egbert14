import numpy as np


def compute_2motors_steam(idsm, m1_range, m2_range, which_plot, num):
    """
    which plot : velocity_term, attraction_term, velocity_attraction
    """
    funcs = {
            'velocity_term': lambda sm: idsm.velocity_term(sm),
            'attraction_term': lambda sm: idsm.attraction_term(sm),
            'velocity_attraction': lambda sm: idsm.velocity_attraction(sm)
            }

    func = funcs[which_plot]

    m1 = np.linspace(m1_range[0], m1_range[1], num=num)
    m2 = np.linspace(m2_range[0], m2_range[1], num=num)
    weights = np.zeros((len(m2), len(m1)))

    influence = np.zeros((len(m2), len(m1), 2))
    for i, m1i in enumerate(m1):
        for j, m2j in enumerate(m2):
            sm_pos = np.array([m1i, m2j])
            influence[j, i, :] = func(sm_pos)
            weights[j, i] = idsm.phi(sm_pos)[0]

    return m1, m2, influence, weights
