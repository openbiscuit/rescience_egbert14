#!/usr/bin/env python3

# External modules
import numpy as np
import matplotlib.pyplot as plt

# You should first run the exp-003.py scripts 
labels = ['%d - %d' % (250*i, 250*(i+1)) for i in range(10)]
phototaxis = np.loadtxt("mean_sq_distances_SimplePhototaxis.data")
sinephototaxis = np.loadtxt("mean_sq_distances_SinePhototaxis.data")
photophobia = np.loadtxt("mean_sq_distances_Photophobia.data")

width = 0.25
x = np.arange(len(labels))
fig, ax = plt.subplots(figsize=(14, 3))
rects1 = ax.bar(x - width, phototaxis,
                width, edgecolor='k',
                facecolor='w',
                label='phototaxis')
rects2 = ax.bar(x, sinephototaxis,
                width, edgecolor='k',
                facecolor='k',
                label='sinusoidal phototaxis')
rects3 = ax.bar(x + width, photophobia,
                width, edgecolor='k',
                facecolor='gray',
                label='Photophobia')

ax.set_xlabel("Time period")
ax.set_ylabel("Mean squared distance to light")
ax.set_ylim(0, 5)
ax.set_xticks(x)
ax.set_xticklabels(labels)
ax.legend()
fig.tight_layout()

plt.savefig("figs/figure7_dist.pdf", bbox_inches="tight")

plt.show()
