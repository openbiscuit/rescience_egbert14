#!/usr/bin/env python3
# coding: utf-8
'''
Reproduction of Figure 5 of
    Modeling habits as self-sustaining patterns of sensorimotor behavior
    M.D. Egbert and X.E. Barandiaran (2014)k
Notes:
    - The legend says the arrows are for the activated IDSM nodes. Were all
      the activated nodes displayed ? If so, it seems we have much more nodes
      than in the paper
'''

# Standard modules
import argparse
import logging
# External modules
import matplotlib.pyplot as plt
import numpy as np
# Local modules
import robots
from history import RobotHistory
from IDSM import IDSM
from progress import ProgressBar
from movie import Movie

parser = argparse.ArgumentParser()
parser.add_argument('--dt',
                    type=float,
                    help='The time step for Euler integration',
                    default=0.01)
parser.add_argument('--Kd',
                    type=float,
                    help='Kd for setting the influence radius of a node',
                    default=12500)
parser.add_argument('--Kw',
                    type=float,
                    help='Kw constant for the weight factor function',
                    default=0.025)
parser.add_argument('--Kt',
                    type=float,
                    help='Kt, the threshold on the density before creating'
                         ' a node',
                    default=1.0)
parser.add_argument('--age_for_active',
                    type=float,
                    help='The time before a nodes influences the motor output',
                    default=10)
parser.add_argument('--T1',
                    type=float,
                    help='The duration of the training period, in s.',
                    default=20.0)
parser.add_argument('--T2',
                    type=float,
                    help='The duration of the second period, in s.',
                    default=15.0)
parser.add_argument('--T3',
                    type=float,
                    help='The duration of the second period, in s.',
                    default=40.0)
parser.add_argument('--logging', choices=["info", "debug"],
                    help='Which logging level to use',
                    default="info")
parser.add_argument('-m', '--movie', action="store_true",
                    default=False,
                    help='Whether to generate a movie or not (it takes time!)')
parser.add_argument('-v', '--verbose', action="store_true",
                    help='Print verbose information')
args = parser.parse_args()

logging.basicConfig(level=logging.INFO if args.logging == "info"
                    else logging.DEBUG)

Kd = args.Kd
Kw = args.Kw
Kt = args.Kt
T1 = args.T1
T2 = args.T2
T3 = args.T3
dt = args.dt
age_for_active = args.age_for_active

idsm = IDSM(nsensors=1, nmotors=1,
            Kd=Kd, Kw=Kw, Kt=Kt,
            age_for_active=age_for_active)
robot = robots.OmniRobot(nsensors=1, nmotors=1)

sval_min = 0.047
sval_max = 0.8
snor_min = 0.12
snor_max = 0.86

def norm_sensor(s):
    return (s-sval_min)/(sval_max-sval_min)*(snor_max-snor_min) + snor_min

def denorm_sensor(s):
    return (s-snor_min)/(snor_max-snor_min)*(sval_max-sval_min) + sval_min

def norm_motor(m):
    return 0.5 + 0.25 * m

def denorm_motor(m):
    return (m - 0.5)/0.25


def get_norm_sm_state(robot):
    s = robot.get_sensors()
    m = robot.get_motors()
    return np.concatenate([norm_sensor(s), norm_motor(m)])


def episode(T0, duration, robot, idsm, get_motor_command, history, movie):
    t = T0
    pbar = ProgressBar(maxval=duration+dt).start()
    while t < T0 + duration:
        # Drive the robot with the
        # provided motor commands
        robot.set_motors(get_motor_command(t))

        # Move the robot
        robot.step(dt)

        # Update the sensors given the position of the robot
        robot.set_sensors(np.array([1.0 / (1.0 + robot.get_position()[0]**2)]))

        # Update the IDSM
        idsm.step(dt, get_norm_sm_state(robot))

        # Record the state of the robot
        history.record(t)

        # Let us display the number of nodes, active nodes, ..
        idsm.debug()

        # Record the movie frame
        if movie:
            movie.record(t)

        t += dt

        logging.debug("t = {}".format(t))
        pbar.update(t - T0)

    pbar.finish()
    
# The forced motor command
forced_motor_command = lambda t: np.array([np.cos(t/2.0)])

# The motor command provided by the IDSM
mu = norm_motor(robot.get_motors())


def idsm_motor_command(t):
    global mu
    mu += dt * idsm.velocity_attraction(get_norm_sm_state(robot))
    return denorm_motor(mu)


# Training
logging.info("Running the simulation for training with external control")

# The object to collect the history of motors, sensors, positions, ..
history_T1 = RobotHistory(robot)

# Create the movie object 
if args.movie:
    movie = Movie(bounds=([-0.5, 0.5], [-5, 0]),
                  robot_history=history_T1,
                  filename='exp-002.mp4',
                  every=10)
else:
    movie = None

# The initial robot position
robot.set_motors(np.zeros(1))
robot.set_position(np.array([-2.5]))

# The motor state is externally provided
motor_command = forced_motor_command

# Run the episode
episode(T0 = 0.0, duration = T1, robot=robot, idsm=idsm,
        get_motor_command=motor_command, history=history_T1, movie=movie)

# Record the active nodes after this period
active_nodes_idx = idsm.nodes['age'] >= idsm.age_for_active
active_nodes_at_T1 = idsm.nodes[active_nodes_idx].copy()

# Second period
logging.info("Running the simulation with IDSM control")

# The object to collect the history of motors, sensors, positions, ..
history_T2 = RobotHistory(robot)
if movie:
    movie.add_history(history_T2)

# The IDSM drives the robot motor state
# Mu is initialized to the current motor state
mu = norm_motor(robot.get_motors())
motor_command = idsm_motor_command

# Run the episode
episode(T1, duration=T2, robot=robot, idsm=idsm,
        get_motor_command=motor_command, history=history_T2, movie=movie)

# Record the active nodes after this period
active_nodes_idx = idsm.nodes['age'] >= idsm.age_for_active
active_nodes_at_T2 = idsm.nodes[active_nodes_idx].copy()

# Last period
logging.info("Relocate the robot and run with IDSM control")

# The object to collect the history of motors, sensors, positions, ..
history_T3 = RobotHistory(robot)
if movie:
    movie.add_history(history_T3)

# We relocalize the robot
# Change the position of the robot and reset its velocity
robot.set_position(np.array([-2.5]))
robot.set_motors(np.zeros(1))

# We need to reset the internals of IDSM for computing the d(sm)/dt
# since it would otherwise be perturbed by the relocalization
idsm.reset_dsm()
# Mu is reset
mu = norm_motor(robot.get_motors())
motor_command = idsm_motor_command

episode(T1+T2, duration=T3, robot=robot, idsm=idsm,
        get_motor_command=motor_command, history=history_T3, movie=movie)

# Record the active nodes after this period
active_nodes_idx = idsm.nodes['age'] >= idsm.age_for_active
active_nodes_at_T3 = idsm.nodes[active_nodes_idx].copy()

# Dump the nodes
idsm.dump_nodes()

# Plots
logging.info("Generating the plots")
pbar = ProgressBar(maxval=6).start()

# Disable logging, otherwise matplotlib generates a lot
logging.disable(logging.DEBUG)

fig = plt.figure(figsize=(10, 10))
ax = plt.subplot(311)
history_T1.plot_motor_history(ax, linestyle='--', color='tab:blue')
history_T2.plot_motor_history(ax, color='tab:orange')
history_T3.plot_motor_history(ax, color='tab:green')
ax.axvline(T1+T2, linestyle='dotted', color='k')
ax.set_ylabel("Denormalized motor state")
pbar.step()

ax = plt.subplot(312)
history_T1.plot_position_history(ax, linestyle='--', color='tab:blue')
history_T2.plot_position_history(ax, color='tab:orange')
history_T3.plot_position_history(ax, color='tab:green')
ax.axvline(T1+T2, linestyle='dotted', color='k')

ax.set_ylim([-5.0, 0.0])
ax.set_xlabel('t (s.)')
ax.set_ylabel('Robot position $x$')
pbar.step()

def plot_sm_space(history_T,
                  active_nodes_at_T,
                  **traj_line_style):
    # Show the trajectory in the SM space for the time up to T
    ax.plot([norm_motor(m) for m in history_T.motors_history],
            [norm_sensor(s) for s in history_T.sensors_history],
            **traj_line_style)
    # Shows the active nodes at T.
    # The color encodes the weights of the nodes at T
    ax.scatter(active_nodes_at_T['position'][:, 1],
               active_nodes_at_T['position'][:, 0],
               c=idsm.weights(active_nodes_at_T['weight']),
               s=10,
               cmap='gray')
    # Show the motor component of the nodes velocity
    for k, (p, v) in enumerate(zip(active_nodes_at_T['position'],
                                   active_nodes_at_T['velocity'])):
        if k % 25 != 0:
            continue
        if args.verbose:
            print(k)
        ax.arrow(p[1], p[0], 0.5*v[1], 0,
                 width=0.01, color='k')
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)
    ax.set_xlabel('normed motor')
    ax.set_ylabel('normed sensor')
    ax.set_aspect('equal')
    ax.set_title('{:.2f} - {:.2f} s'.format(history_T.times[0],
                                            history_T.times[-1]))

# Save the movie
if args.movie:
    movie.save()

# Show the nodes after 15s.
ax = plt.subplot(337)
plot_sm_space(history_T1, active_nodes_at_T1, linestyle='--', color='tab:blue')
pbar.step()

ax = plt.subplot(338)
plot_sm_space(history_T2, active_nodes_at_T2, color='tab:orange')
pbar.step()

ax = plt.subplot(339)
plot_sm_space(history_T3, active_nodes_at_T3, color='tab:green')
pbar.step()

fig.tight_layout()

plt.savefig('figs/figure5.pdf', bbox_inches='tight')
logging.info('figs/figure5.pdf saved'.format(Kd, dt))
pbar.step()
pbar.finish()
plt.show()
