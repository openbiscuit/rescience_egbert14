# Standard modules
import logging
# External modules
import numpy as np


class IDSM:

    def __init__(self, nsensors, nmotors, Kd, Kw, Kt, age_for_active):
        self.nsensors = nsensors
        self.nmotors = nmotors
        self.sm_pos_1 = None
        self.Kd = Kd
        self.Kw = Kw
        self.Kt = Kt

        self.age_for_active = age_for_active

        self.dim_sm = self.nsensors + self.nmotors
        self.dtype = np.dtype([
            ('position', 'f4', (self.dim_sm, )),
            ('velocity', 'f4', (self.dim_sm, )),
            ('weight', 'f4'),
            ('age', 'f4')
        ])

        # Unitialized list of 0 entries
        self.nodes = np.empty(0, dtype=self.dtype)

        self.reset_dsm()

    def copy(self, base_idsm):
        self.nodes = base_idsm.nodes.copy()

    def RBF(self, sm_pos, nodes_positions=None):
        if nodes_positions is None:
            nodes_positions = self.nodes['position']
        l2 = np.sum((nodes_positions - sm_pos)**2, axis=1)
        coeff = np.exp(-self.Kd * l2)
        dists = 2.0 * coeff / (coeff + 1.0)
        return dists

    def weights(self, w=None):
        # Weights is between 0.0 and 2.0
        # 0.0 when weight << 0.0
        # 1.0 when weight == 0.0
        # 2.0 when weight >> 0.0
        if w is None:
            w = self.nodes['weight']
        return 2.0 / (1.0 + np.exp(-self.Kw * w))

    def phi(self, sm_pos, nodes=None):
        if nodes is None:
            nodes = self.nodes

        # The weights of the nodes, constrained to [0.0, 2.0]
        weights = self.weights(nodes['weight'])

        # The RBF centered on each node (see Fig1 A)
        dists   = self.RBF(sm_pos, nodes['position'])

        return np.sum(weights * dists), dists

    def velocity_term(self, sm_pos):
        # Weights the velocity of the motor components
        # commands as suggested by the neighbooring nodes
        # This will enforce the current motor command to vary in
        # the same direction as the neighbooring active nodes

        active_nodes = self.nodes[self.nodes['age'] > self.age_for_active]

        weights    = self.weights(active_nodes['weight'])
        dists      = self.RBF(sm_pos, active_nodes['position'])
        velocities = active_nodes['velocity'][:, self.nsensors:]

        return np.sum(np.multiply(weights * dists, velocities.T).T, axis=0)

    def attraction_term(self, sm_pos):
        active_nodes = self.nodes[self.nodes['age'] > self.age_for_active]
        # Compute the vectors pointing toward the nodes
        a = (active_nodes['position'] - sm_pos)

        # Remove the component aligned with the velocity
        # a - (<a, Nv/||Nv||> Nv/||Nv||)
        velocities = active_nodes['velocity']
        velocity_norms = np.linalg.norm(velocities,
                                        ord=2,
                                        keepdims=True,
                                        axis=1)
        # If the velocity norm is null, the attraction term is "a"
        # otherwise remove the part colinear to velocity
        # Below we artificially set the norm of null velocity to an arbitrary
        # value (1.0). Since Nv is null, the result is unchanged.
        # This is necessary to preserve the vectorized nature of the
        # operation.
        velocity_norms[velocity_norms == 0] = 1
        velocity_vectors_unitary = velocities/velocity_norms
        a_dot_v = np.sum(a * velocity_vectors_unitary, axis=1)
        a = (a - np.multiply(a_dot_v, velocity_vectors_unitary.T).T)[:, self.nsensors:]

        # Weights all the contributions by the weights
        weights = self.weights(active_nodes['weight'])
        dists   = self.RBF(sm_pos, active_nodes['position'])

        return np.sum(np.multiply(weights * dists, a.T).T, axis=0)

    def dump_nodes(self, smpos=None):
        '''
        Save the current state of the IDSM
        '''
        # Clean the file
        f = open('debug_idsm.txt', 'w')
        f.close()
        f = open('debug_idsm.txt', 'a')

        if smpos is not None:
            f.write("The smpos was {}\n\n".format((smpos)))

        f.write("{} nodes\n".format(len(self.nodes)))
        f.write("="*80 + "\n")
        f.write("Positions :\n")
        np.savetxt(f, self.nodes['position'])

        f.write("="*80 + "\n")
        f.write("Velocities :\n")
        np.savetxt(f, self.nodes['velocity'])

        f.write("="*80 + "\n")
        f.write("Weights :\n")
        np.savetxt(f, self.nodes['weight'])

        f.write("="*80 + "\n")
        f.write("Ages :\n")
        np.savetxt(f, self.nodes['age'])
        f.close()
        print("The nodes are saved in debug_idsm.txt")


    def velocity_attraction(self, sm_pos):
        '''
        Computes the fusion of the velocity and attraction terms

        Arguments:

            sm_pos: a position in the sensorimotor space a vector of size
                self.nsensors + self.nmotors
        Returns:
            A vector of size (self.nmotors,) which is the righthandside term
                of equation (10) in the paper.
        '''
        active_nodes = self.nodes[self.nodes['age'] > self.age_for_active]
        phi, dists = self.phi(sm_pos, active_nodes)
        attraction_term = self.attraction_term(sm_pos)
        velocity_term = self.velocity_term(sm_pos)
        logging.debug("Phi : {}".format(phi))
        logging.debug("Attraction term : {}".format(attraction_term))
        logging.debug("Velocity term : {}".format(velocity_term))
        if phi < np.finfo(float).tiny:
            logging.debug("The density is nearly null...")
            logging.debug("  Phi={}".format( phi ))
            return np.zeros_like( velocity_term )
        
        return 1.0 / phi * (attraction_term + velocity_term)

    def get_motor_command(self, sm_pos):
        return self.velocity_attraction(sm_pos)

    def create_node(self, pos, vel):
        # if np.linalg.norm(vel) == 0:
        #     logging.warning("Not creating a node with a null"
        #                     " velocity")
        #     return
        if np.linalg.norm(vel)**2 >= 10.0:
            logging.warning(f"Not creating a node before vel({np.linalg.norm(vel)}) > 10")
            return
        new_node = np.zeros(1, self.dtype)
        new_node['position'] = pos
        new_node['velocity'] = vel
        new_node['weight'] = 0.0
        new_node['age'] = 0.0
        # Append creates a new array... several copies
        # will occur if we create several nodes
        self.nodes = np.append(self.nodes, new_node)

    def reset_dsm(self):
        self.sm_pos_1 = None

    def step(self, dt, sm_pos):

        if self.sm_pos_1 is None:
            self.sm_pos_1 = sm_pos.copy()
            return

        # Increment the age of all the nodes
        self.nodes['age'] += dt

        # Computes the density at sm_pos
        phi, dists = self.phi(sm_pos)

        # Update the weights
        self.nodes['weight'] += dt * (-1.0 + 10.0 * dists)

        # Do we need to create new nodes ?
        if phi < self.Kt:
            dsm_pos = (sm_pos - self.sm_pos_1)/dt
            self.create_node(sm_pos, dsm_pos)

        self.sm_pos_1 = sm_pos.copy()

    def compute_motor_command(self, sm_pos):
        return np.zeros(self.nmotors)

    def debug(self):
        min_w = '-'
        max_w = '-'
        if len(self.nodes) > 0:
            min_w = np.min(self.nodes['weight'])
            max_w = np.max(self.nodes['weight'])
        active_nodes = self.nodes['age'] > self.age_for_active
        msg = """Number of nodes : {}; Active : {} , weights range : [{} ; {}]""".format(len(self.nodes), np.sum(active_nodes), min_w, max_w)
        logging.debug(msg)
        if len(self.nodes) > 0 and (np.isnan(min_w) or np.isnan(max_w)):
            raise RuntimeError("Some weights are Nan !")
