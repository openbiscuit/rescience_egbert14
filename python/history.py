# Standard modules
from enum import Enum, auto
from typing import List
# External modules
import numpy as np
# Local modules


class Padding(Enum):
    NAN = auto()
    CONSTANT = auto()


class RobotHistory:

    def __init__(self, robot):
        self.robot = robot
        self.motors_history = []
        self.sensors_history = []
        self.position_history = []
        self.times = []
        # Register the indexes when there is a cut in a sequence
        # of recordings. E.g. when the robot is relocated or
        # when the robot go through the bounds of the arena
        self.break_idx = []

    def record(self, t):
        self.motors_history.append(self.robot.get_motors())
        self.sensors_history.append(self.robot.get_sensors())
        self.position_history.append(self.robot.get_position())
        self.times.append(t)

    def record_break(self):
        self.break_idx.append(len(self.times))

    def break_serie(self, src_list: List,
                    pad: Padding):
        '''
        Inserts into a copy of the provided list some nan to indicate
        when there was a break in the sequence of elements in src_list

        '''
        # If there is no break, just return the original list
        # We return a copy to be consistent with the second case below
        if not self.break_idx:
            return src_list.copy(), None

        dst_list = []
        start_idx = 0
        if isinstance(src_list[0], float) or isinstance(src_list[0], int):
            pad_f = lambda v: v
        elif isinstance(src_list[0], np.ndarray):
            num_elem = src_list[0].size
            pad_f = lambda v: np.array([np.nan] * num_elem)
        else:
            raise NotImplementedError("Cannot pad with other things than"
                                      " ndarray or float. Got {}".format(src_list[0]))
        b = 0
        for b in self.break_idx:
            if b == 0:
                continue
            dst_list += src_list[start_idx:b]
            if pad == Padding.CONSTANT:
                dst_list.append(pad_f(dst_list[-1]))
            else:
                dst_list.append(pad_f(np.nan))
            start_idx = b

        # There might be points remaining in src_list
        if b != (len(src_list) - 1):
            dst_list += src_list[b:]
        return (dst_list, src_list[b:])

    def plot_motor_history(self, ax, *style, **kwargs):
        # Build the series with a cut (i.e. a Nan) on break_idx
        ts, _ = self.break_serie(self.times, Padding.CONSTANT)
        motors = np.array(self.break_serie(self.motors_history, Padding.NAN)[0])
        ax.plot(ts, motors, *style, **kwargs)

    def plot_sensor_history(self, ax, *style, **kwargs):
        # Build the series with a cut (i.e. a Nan) on break_idx
        ts, _ = self.break_serie(self.times, Padding.CONSTANT)
        sensors = np.array(self.break_serie(self.motors_history, Padding.NAN)[0])
        ax.plot(ts, sensors, *style, **kwargs)

    def plot_position_history(self, ax, *style, **kwargs):
        # Build the series with a cut (i.e. a Nan) on break_idx
        ts, _ = self.break_serie(self.times, Padding.CONSTANT)
        positions = np.array(self.break_serie(self.position_history,
                                              Padding.NAN)[0])

        if len(positions.shape) == 1:
            # 1D position
            ax.plot(ts, positions, *style, **kwargs)
        else:
            if positions.shape[1] == 2:
                # 2D environnements
                ax.plot(positions[:, 0], positions[:, 1],
                        *style, **kwargs)
            else:
                ax.plot(ts, positions, *style, **kwargs)

    def split_position_history(self, timelapse):
        '''
        Builds a generator for iterating over chunks of the position
        history with timelength of timelapse.
        '''
        start_idx = 0
        # The list "times" is padded by repeating the last time
        # at the break index  instead of inserting a Nan because
        # we need monotically increasing times for splitting the series
        times, _ = self.break_serie(self.times, Padding.CONSTANT)
        positions, _ = self.break_serie(self.position_history, Padding.NAN)
        num_elements = len(times)
        end_idx = -1
        while start_idx < num_elements:
            # Find the smallest index larger than start_idx so that
            # times[end_idx] >= times[start_idx] + timelapse
            t0 = times[start_idx]
            tf = t0 + timelapse
            for delta_idx, t in enumerate(times[start_idx:]):
                if t > tf:
                    break
            end_idx = start_idx + delta_idx + 1
            yield times[start_idx:end_idx], positions[start_idx:end_idx]
            start_idx = end_idx

    def get_last_positions(self, delta_time):
        '''
        Return the positions for the last 'delta_time' times.
        '''
        # last time
        last_time = self.times[-1]
        # traverse self.times in reversed order
        for idx, t in reversed(list(enumerate(self.times))):
            if t < last_time - delta_time:
                return self.position_history[idx:]
        return self.position_history
